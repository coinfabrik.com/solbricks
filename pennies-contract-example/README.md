# Install Solana Tool Suite and Anchor
[Install Solana tool suite following this instructions](https://docs.solana.com/cli/install-solana-cli-tools)

[Install Anchor following this instructions](https://book.anchor-lang.com/getting_started/installation.html)
# Build
1.  Run  `anchor build`. Your program keypair is now in  `target/deploy`. 
2.  Run  `anchor keys list`  to display the keypair's public key and copy it into your  `declare_id!`  macro at the top of  `lib.rs`.
3.  Run  `anchor build`  again. This step is necessary to include the new program id in the binary.
# Test
Run  `anchor test`
# Deploy
1.  Run  `solana-test-validator`
2.  Run  `anchor deploy`
3.  Run  `anchor idl init --filepath ./target/idl/pennies.json <program keypair>`