import * as anchor from "@coral-xyz/anchor";
import { Program } from "@coral-xyz/anchor";
import { Pennies } from "../target/types/pennies";
import { assert } from "chai";

describe("pennies", () => {
  // Configure the client to use the local cluster.
  const provider = anchor.AnchorProvider.local(undefined, {
    commitment: `confirmed`,
  });
  anchor.setProvider(provider);

  const program = anchor.workspace.Pennies as Program<Pennies>;

  const player = anchor.web3.Keypair.generate();
  const player2 = anchor.web3.Keypair.generate();
  const game = anchor.web3.Keypair.generate();
  const game2 = anchor.web3.Keypair.generate();
  const game3 = anchor.web3.Keypair.generate();

  before(async () => {
    // Fund player
    const tx = new anchor.web3.Transaction();

    tx.add(
      anchor.web3.SystemProgram.transfer({
        fromPubkey: provider.wallet.publicKey,
        lamports: 200_000_000,
        toPubkey: player.publicKey,
      })
    );
    tx.add(
      anchor.web3.SystemProgram.transfer({
        fromPubkey: provider.wallet.publicKey,
        lamports: 200_000_000,
        toPubkey: player2.publicKey,
      })
    );
    tx.add(
      anchor.web3.SystemProgram.transfer({
        fromPubkey: provider.wallet.publicKey,
        lamports: 200_000_000,
        toPubkey: game.publicKey,
      })
    );

    await provider.sendAndConfirm(tx, undefined, { commitment: `confirmed` });
  });

  it("Creates game as odd", async () => {
    const tx = await program.methods
      .initGame(true, true)
      .accounts({
        challenger: player.publicKey,
        game: game.publicKey
      })
      .signers([
        player,
        game
      ])
      .rpc({
        commitment: "confirmed"
      });
    
    const res = await provider.connection.getParsedTransaction(tx, {
        commitment: `confirmed`,
      });

    
    const acc = await program.account.game.fetch(game.publicKey, "confirmed");

    assert(acc.oddValue == true);
    assert(acc.state.evenTurn);
  });

  it("Play as even and win", async () => {
    const tx = await program.methods
      .play(true)
      .accounts({
        player: player2.publicKey,
        game: game.publicKey
      })
      .signers([
        player2,
        game
      ])
      .rpc({
        commitment: "confirmed"
      });
    
    const res = await provider.connection.getParsedTransaction(tx, {
        commitment: `confirmed`,
      });

    const acc = await program.account.game.fetch(game.publicKey, "confirmed");

    assert(acc.oddValue == true);
    assert(acc.evenValue == true);
    assert(acc.state.evenWins);
  });

  it("Creates game as even", async () => {
    const tx = await program.methods
      .initGame(true, false)
      .accounts({
        challenger: player.publicKey,
        game: game2.publicKey
      })
      .signers([
        player,
        game2
      ])
      .rpc({
        commitment: "confirmed"
      });
    
    const res = await provider.connection.getParsedTransaction(tx, {
        commitment: `confirmed`,
      });

    
    const acc = await program.account.game.fetch(game2.publicKey, "confirmed");

    assert(acc.evenValue == true);
    assert(acc.state.oddTurn);
  });

  it("Play as odd and win", async () => {
    const tx = await program.methods
      .play(false)
      .accounts({
        player: player2.publicKey,
        game: game2.publicKey
      })
      .signers([
        player2,
        game2
      ])
      .rpc({
        commitment: "confirmed"
      });
    
    const res = await provider.connection.getParsedTransaction(tx, {
        commitment: `confirmed`,
      });

    const acc = await program.account.game.fetch(game2.publicKey, "confirmed");

    assert(acc.oddValue == false);
    assert(acc.evenValue == true);
    assert(acc.state.oddWins);
  });
});
