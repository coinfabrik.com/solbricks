use anchor_lang::prelude::*;

#[error_code]
pub enum Error {
    GameNonInitialized,
    GameAlreadyInitialized,
    GameAlreadyFinished,
}
