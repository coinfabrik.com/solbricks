use anchor_lang::prelude::*;

use crate::state::Game;

pub fn handler(ctx: Context<Play>, value: bool) -> Result<()> {
    ctx.accounts.game.play(value)
}

#[derive(Accounts)]
pub struct Play<'info> {
    pub player: Signer<'info>,

    #[account(mut, signer)]
    pub game: Account<'info, Game>,
}
