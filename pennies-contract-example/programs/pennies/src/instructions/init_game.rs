use anchor_lang::prelude::*;

use crate::state::Game;

pub fn handler(ctx: Context<InitGame>, value: bool, odd: bool) -> Result<()> {
    ctx.accounts.game.start_game(value, odd)
}

#[derive(Accounts)]
pub struct InitGame<'info> {
    #[account(mut)]
    pub challenger: Signer<'info>,
    #[account(
        init,
        payer = challenger,
        space = Game::LEN + 8
    )]
    pub game: Account<'info, Game>,

    pub system_program: Program<'info, System>,
}
