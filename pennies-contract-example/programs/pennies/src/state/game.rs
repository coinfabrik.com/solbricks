use anchor_lang::prelude::*;

use crate::errors::Error;

#[derive(AnchorSerialize, AnchorDeserialize, Clone, PartialEq, Eq)]
pub enum GameState {
    NonInitialized,
    OddTurn,
    EvenTurn,
    OddWins,
    EvenWins,
}

#[account]
pub struct Game {
    pub state: GameState,
    pub odd_value: bool,
    pub even_value: bool,
}

impl Game {
    pub const LEN: usize = 8 + 1 + 1 + 1;

    pub fn start_game(&mut self, val: bool, odd: bool) -> Result<()> {
        if self.state != GameState::NonInitialized {
            return Err(Error::GameAlreadyInitialized.into());
        }
        match odd {
            true => {
                self.odd_value = val;
                self.state = GameState::EvenTurn;
            }
            false => {
                self.even_value = val;
                self.state = GameState::OddTurn;
            }
        };
        return Ok(());
    }
    pub fn play(&mut self, val: bool) -> Result<()> {
        match self.state {
            GameState::NonInitialized => {
                return Err(Error::GameNonInitialized.into());
            }
            GameState::EvenWins | GameState::OddWins => {
                return Err(Error::GameAlreadyFinished.into());
            }
            GameState::EvenTurn => {
                self.even_value = val;
            }
            GameState::OddTurn => {
                self.odd_value = val;
            }
        };
        if self.even_value == self.odd_value {
            self.state = GameState::EvenWins;
        } else {
            self.state = GameState::OddWins;
        }
        return Ok(());
    }
}
