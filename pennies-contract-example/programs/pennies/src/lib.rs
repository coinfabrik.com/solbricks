use anchor_lang::prelude::*;

declare_id!("9Q1dFrthAfYvvqoMSMqwtqWm1rnByEVfmR9zX8351bTa");

pub mod errors;
pub mod instructions;
pub mod state;

pub use crate::instructions::*;

#[program]
pub mod pennies {
    use super::*;

    pub fn init_game(ctx: Context<InitGame>, value: bool, odd: bool) -> Result<()> {
        init_game::handler(ctx, value, odd)
    }

    pub fn play(ctx: Context<Play>, guess: bool) -> Result<()> {
        play::handler(ctx, guess)
    }
}
